#include <stdio.h>
#include <stdlib.h>

void fun1(int arg)
{
    printf("before function call: %d\n", arg);
    arg = arg + 1;
}
void fun2(int *arg)
{
    printf("before function call: %d\n", *arg);
    *arg = *arg + 1;
}
struct UserType {
    int part1;
    char *part2 ;
};

int main(int argc, char *argv[])
{
    printf("\n\n");
    /***********************
    *  Intro to Pointers  *
    ***********************/
    /*
     *demonstrates how to declare, assign,
     *and use the '*',  'value of', with Pointers
     */
    int var1 = 96;
    int *pointer1 = &var1;
    printf("0 Intro to Pointers: demonstrates how to declare, assign, and use the '*',  'value of', with Pointers\n");
    printf("var1: %d === %d :*pointer1\n", var1, *pointer1);
    printf("\n\n");


    /***************
    *  int arrys  *
    ***************/

    // *****    printf
    printf("1 int.\n");
    printf("*****int arrays\n");
    printf("**array construction, notice the garbage in array[0] since was not initialized\n");
    
    // *****    code
    int array1[9];
    /*int array1[]; // requieres size or initializing value*/
    array1[7] = 96;

    // *****    printf
    printf( "array1[7]: %d === %d :*(array1 + 7)\n", array1[7] , *(array1 + 7) );
    printf( "array1[0]: %d === %d :*array1\n", array1[0] , *array1 );
    printf("here is the out to print the address the pointer array1 has: %p\n", array1);
    printf("\n");


    /******************
    *  int pointers  *
    ******************/
    
    // *****    printf
    printf("******int pointers\n");
    printf("**pointer construction, notice in array[0] malloc assign all elements to 0\n");

    // *****    code
    int *pointer2;
    pointer2 = ( int * )malloc( 9 * sizeof(int) );
    *(pointer2 + 7) = 96;

    // *****    printf
    printf( "pointer2[7]: %d === %d :*(pointer2 + 7)\n", pointer2[7] , *(pointer2 + 7) );
    printf( "pointer2[0]: %d === %d :*pointer2\n", pointer2[0] , *pointer2 );
    printf("here is out to print the address the pointer pointer2 has: %p\n", pointer2);
    printf("\n\n");


    /*****************
    *  char arrays  *
    *****************/
    
    // ****** printf
    printf("2 char:\n");
    printf("\n");
    printf("*****char arrays\n");
    printf("\n");
    printf("**array construction, notice the garbage in array[0] since was not initialized\n");
    printf("**string with array contruction, i.e. \"\"\n");
    printf("\n");

    // ****** code
    /*char string1[10] = "Some Text";*/
    char string1[] = "Some Text"; // must have either size or initializatoin
    /*char string2[10] = { 'S' , 'o' , 'm' , 'e' , ' ' , 'T' , 'e' , 'x' , 't' , '\0' };*/
    char string2[] = { 'S' , 'o' , 'm' , 'e' , ' ' , 'T' , 'e' , 'x' , 't' , '\0' };

    // ****** printf
    printf("string1: %s\n", string1);
    printf("**this was contructed with individual char '' elements vs a whole string \"\" as in string1\n");
    printf("\n");
    printf("string2: %s \n", string2);
    printf("\n");


    /*******************
    *  char pointers  *
    *******************/
    
    // ****** printf
    printf("*****char pointers\n");
    printf("\n");
    printf("**string with pointer contuction, notice initialization is different for string contuction with ''\n");

    // ****** code
    char *string3;
    string3 = ( char *  )malloc( 10 * sizeof(char) );
    /*cannot be initialized this way below as was above:*/
    /*says redefinition of type from *string3 to string[10]*/
    /*char string3[] = { 'S' , 'o' , 'm' , 'e' , ' ' , 'T' , 'e' , 'x' , 't' , '\0' };*/
    string3[0] = 'S';
    string3[1] = 'o';
    string3[2] = 'm';
    string3[3] = 'e';
    string3[4] = ' ';
    string3[5] = 'T';
    string3[6] = 'e';
    string3[7] = 'x';
    string3[8] = 't';
    string3[9] = '\0';

    // ****** printf
    printf("string3: %s\n", string3);
    printf("**however notice with the int array alone produces address but the char array produce its values starting at [0]\n");
    printf( "array1: %p \n", array1 );
    printf("\n\n");

    /**********************************
    *  Pointers used with Functions  *
    **********************************/
    printf("3 Pointers used with Functions\n");
    printf("**passing by value\n");
    fun1( var1 );
    printf("after function call: %d\n", var1);
    printf("**passing by reference\n");
    fun2( &var1 );
    printf("after function call: %d\n", var1);
    printf("\n\n");


    /***********************************
    *  Pointer used for Optimization  *
    ***********************************/
    printf("4 Pointer used for Optimization\n");
    // ******   code
    struct UserType var3;
    var3.part2 = ( char * )malloc( 10 * sizeof(char) ); // without size will get Segmentation fault
    var3.part2[0] = 'S';
    var3.part2[1] = 'o';
    var3.part2[2] = 'm';
    var3.part2[3] = 'e';
    var3.part2[4] = ' ';
    var3.part2[5] = 'T';
    var3.part2[6] = 'e';
    var3.part2[7] = 'x';
    var3.part2[8] = 't';
    var3.part2[9] = '\0';

    // ******   printf
    printf("%s\n", var3.part2);

    // ******   code
    struct UserType *pointer3;
    pointer3 = &var3;

    pointer3 -> part1 = 96;
    /*( *pointer3 ).part1 = 96; // pointer, less readable way to write*/

    // *****    printf
    printf("%d\n", var3.part1);
    
    return 0;
}
